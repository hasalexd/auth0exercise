# auth0exercise
<h1> Architecture </h1>

<h1> auth0api </h1>
This is the API component. Currently it has one function "menu.py".
It is designed to run on lambda with API Gateway fronting it.

<h2> Deploy </h2>
Super manual at the moment. Next step would be to automate this with Zappa or Serverless.
Investigated using AWS SAM, but apparently it doesn't play well with pipenv (?) so, went the manual route.
Current issues with packages are likely due to this.

<h1> auth0frontend </h1>
This is the SPA front end. It handles auth, and ordering. There is a stubbed out page for profile, but this is non-functional.

<h2> Running </h2>
`npm start` will do all you need here.

<h2> Building </h2>
`npm run build` will webpack and uglify the code into the build folder.

<h1> Auth0 Rules </h1>
These are rules/functions for use within the Auth0 platform.
To link profiles I used the default rule for doing so
`Link Accounts with Same Email Address while Merging Metadata`

For pulling info from the People API (stub code) I used the following:
```function (user, context, callback) {
  if (context.connection !== 'google') {
    return callback(null, user, context);
  }

  const request = require('request');

  const gIdentity = _.find(user.identities, { connection: 'google-oauth2' });

  const options = {
    url: 'https://people.googleapis.com/v1/resourceName=people/'+gIdentity.user_id+'/connections?personFields=emailAddresses',
    headers: {
      Authorization: 'Bearer ' + gIdentity.access_token
    },
    json: true
  };

  request(options, function (error, response, body) {
    if (error) return callback(error);
    if (response.statusCode !== 200) return callback(new Error(body));

    if (body.connections) {
      context.idToken.connection_count = body.totalItems;
    }

    return callback(null, user, context);
  });
}```

<h1> Known Issues </h1>

1) Auth Weirdness. Passing the Auth as a prop seems to require reinstantiation on each subsequent component (see: needing to click the login button on the home page). However reinstantiating from local storage doesn't appear to totally work

2) API Gremlins: Currently having issues with the cryptography package. I am reworking the build of it to better match the lambda environment. If this doesn't work, I will need to move to a docker image on ECS/EKS.

3) Google People API access: To access the contact count from the google people api, the app must be independently verified by Google. This has not been done, as this is a demo app and does not need a full review from Google.