import React, { Component } from "react";
import { Navbar, Button, Nav, NavItem } from 'react-bootstrap';


class CustomNavBar extends Component {

    goTo(route) {
        this.props.history.replace(`/${route}`)
    }
      
    login() {
    this.props.auth.login();
    }

    logout() {
    this.props.auth.logout();
    }

    componentDidMount() {
    const { renewSession } = this.props.auth;

    if (localStorage.getItem('isLoggedIn') === 'true') {
        renewSession();
    }
    }
    isAuthenticated(){
        return this.props.auth.isAuthenticated();
    }

    render(){
        return(
        <div>
        <Navbar fluid>
        <Navbar.Header>
            <Navbar.Brand>
            A Pizza Place
            </Navbar.Brand>
            <Nav>
            <NavItem eventKey={1} href="/home" >Home</NavItem>
            <NavItem eventKey={2} href="/profile">Profile</NavItem>
            <NavItem eventKey={3} href="/order">Order</NavItem>
            </Nav>
        </Navbar.Header>
        <Nav pullRight>

            {
            !this.isAuthenticated() && (
                <Button
                    id="qsLoginBtn"
                    bsStyle="primary"
                    className="btn-margin"
                    onClick={this.login.bind(this)}
                >
                    Log In
                </Button>
                )
            }
            {
            this.isAuthenticated() && (
                <Button
                    id="qsLogoutBtn"
                    bsStyle="primary"
                    className="btn-margin"
                    onClick={this.logout.bind(this)}
                >
                    Log Out
                </Button>
                )
            }
            </Nav>
        </Navbar>
        </div>
        )
        }
    }
export default CustomNavBar;