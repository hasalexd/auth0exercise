import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Home extends Component {
  
login() {
  this.props.auth.login();
}

componentDidMount() {
const { renewSession } = this.props.auth;

if (localStorage.getItem('isLoggedIn') === 'true') {
    renewSession();
}
}
isAuthenticated(){
    return this.props.auth.isAuthenticated();
}
  render() {
    return (
      <div className="container">
        {
          this.isAuthenticated() && (
              <h4>
                You are logged in! You can now order Pizza!{' '}
                <Link to="order">order here</Link>
                .
              </h4>
            )
        }
        {
          !this.isAuthenticated() && (
              <h4>
                You are not logged in! Please{' '}
                <a
                  style={{ cursor: 'pointer' }}
                  onClick={this.login.bind(this)}
                >
                  Log In
                </a>
                {' '}to continue.
              </h4>
            )
        }
      </div>
    );
  }
}

export default Home;
