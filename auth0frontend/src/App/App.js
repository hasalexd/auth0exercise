import React, { Component } from "react";
import pizza from './pizza_image.jpg';

class App extends Component {


  goTo(route) {
    this.props.history.replace(`/${route}`)
  }
  
  login() {
    this.props.auth.login();
  }

  logout() {
    this.props.auth.logout();
  }

  componentDidMount() {
    const { renewSession } = this.props.auth;

    if (localStorage.getItem('isLoggedIn') === 'true') {
      renewSession();
    }
  }

  isAuthenticated(){
    this.props.auth.isAuthenticated();
  }

  render() {
    return (
      <div className="content">
      <img src={pizza} alt="A Pizza Place"/>
      </div>
  );
  }
}
 
export default App;