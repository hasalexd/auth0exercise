import React, { Component } from 'react';
import {Form, Checkbox} from 'react-form'
import axios from 'axios';


export default class Order extends Component{

getEmailStatus(){
    return this.props.auth.checkEmailStatus();
}

getCurrentMenu(){
    const instance = axios.create({
        baseURL: 'https://api.pizza.alexdenttest.com/prod/menu/',
        timeout: 10000,
        headers: {'authentication': this.props.auth.getAccessToken()}
      });
      const response = instance.get();
      if (response.status !== 200){
        console.log(response);
        return null;
      }
    return response;
}
getToppings(){
    const instance = axios.create({
        baseURL: 'https://api.pizza.alexdenttest.com/toppings/',
        timeout: 10000,
        headers: {'authentication': this.props.auth.getAccessToken()}
      });
      const response = instance.get();
    return response;
}
postOrder(values){
    const instance = axios.create({
        baseURL: 'https://api.pizza.alexdenttest.com/prod/order/',
        timeout: 10000,
        headers: {'authentication': this.props.auth.getAccessToken()},
        data: {values}
      });
      const response = instance.post();
      if (response.status !== 200){
        console.log(response);
        return null;
      }
    return response;
}

generateMenu = (jsonMenu) => {
    var listItems;
    if (jsonMenu == null && this.getEmailStatus()){
        listItems = "Check back later. Our system is having issues.";
    }
    else if (jsonMenu == null && this.getEmailStatus()===false){
        listItems = "Please verify your email while we fix our system.";
    }
    else{
        if (this.getEmailStatus()){
            listItems = jsonMenu.map((item) => {
                return(
                    <div>
                    <Form onSubmit={submittedValues => this.setState({ submittedValues })}>
                    {formAPI=>(
                    <form onSubmit={formAPI.submitForm} id="form2">
                    <label htmlFor="menu">Menu</label>
                    <label htmlFor={item.name} className="mr-2">{item.name}</label>
                    <label htmlFor={item.description} className="mr-2">{item.description}</label>
                    <label htmlFor="price" className="mr-2">{item.price}</label>
                    <Checkbox field={item.name} id={item.name} className="d-inline-block" />
                    <button type="submit" className="mb-4 btn btn-primary">
                    Submit
                    </button>
                    </form>
                    )};
                    </Form>
                    </div>
                );
            });
        }
        else{
                listItems = jsonMenu.map((item) => {
                    return(
                        <div>
                        <Form onSubmit={submittedValues => this.setState({ submittedValues })}>
                        {formAPI=>(
                        <form>
                        <label htmlFor="menu">Menu</label>
                        <label htmlFor={item.name} className="mr-2">{item.name}</label>
                        <label htmlFor={item.description} className="mr-2">{item.description}</label>
                        <label htmlFor="price" className="mr-2">{item.price}</label>
                        <Checkbox field={item.name} id={item.name} className="d-inline-block" />
                        <label htmlFor="Email Warning" className="mr-2">"Please Verify your Email before Ordering."</label>
                        </form>
                        )};
                        </Form>
                        </div>
                    );
                });
        }
    }
return listItems;
}


render(){
    var jsonMenu = this.getCurrentMenu();
    return(
        <div className='menu'>
            {this.generateMenu(jsonMenu)}
        </div>
    )
    }
}