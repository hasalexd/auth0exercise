import React, { Component } from 'react';
import loading from './loading.svg';
import {Redirect} from 'react-router-dom';

class Callback extends Component {

  renderRedirect(){
    return <Redirect to='/home'/>
  }
  setRedirect = () => {
    setTimeout(this.setState({
      redirect: true
    }), 3000)
  }

  render() {
    const style = {
      position: 'absolute',
      display: 'flex',
      justifyContent: 'center',
      height: '100vh',
      width: '100vw',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: 'white',
    }

    return (
      <div style={style}>
        <img src={loading} alt="loading"/>

        
        {this.setRedirect()}
        {this.renderRedirect()}
      </div>

    );
  }
}

export default Callback;