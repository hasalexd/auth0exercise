import history from '../history';
import auth0 from 'auth0-js';
import {AUTH_CONFIG} from './auth0-config-prod.json';

export default class Auth {
  accessToken;
  idToken;
  expiresAt;

  auth0 = new auth0.WebAuth({
    audience: AUTH_CONFIG.audience,
    scope: AUTH_CONFIG.scope,
    domain: AUTH_CONFIG.domain,
    clientID: AUTH_CONFIG.clientId,
    redirectUri: AUTH_CONFIG.callbackUrl,
    responseType: 'token id_token'
  });

  constructor() {
    console.log(AUTH_CONFIG);
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.handleAuthentication = this.handleAuthentication.bind(this);
    this.isAuthenticated = this.isAuthenticated.bind(this);
    this.getAccessToken = this.getAccessToken.bind(this);
    this.getIdToken = this.getIdToken.bind(this);
    this.renewSession = this.renewSession.bind(this);
  }
  /*
    Login Function
    Triggers the auth0 login
  */
  login() {
    this.auth0.authorize();
  }

  handleAuthentication() {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
      } else if (err) {
        console.log(authResult);
        history.replace('/home');
        console.log(err);
        alert(`Error: ${err.error}. Check the console for further details.`);
      }
    });
  }

  getAccessToken() {
    if (this.accessToken == null){
      this.loadSession();
    }
    
    return localStorage.getItem('accessToken');
  }

  getIdToken() {
    if (this.idToken == null){
      this.loadSession();
    }
    return localStorage.getItem('idToken');
  }

  getUserInfo() {
    // call auth0 to get userInfo
    if (this.accessToken == null){
      this.loadSession();
    }
    this.auth0.client.userInfo(this.accessToken, function(err, user) {
      console.log(user);
      return user;
    });
  }
  getProfile(cb) {
    if (this.accessToken == null){
      this.loadSession();
    }
    this.auth0.client.userInfo(this.accessToken, (err, profile) => {
      cb(err, profile);
    });
  }

  checkEmailStatus() {
    if (this.accessToken == null){
      this.loadSession();
    }
    // quick case to prevent unnecessary work.
    if (localStorage.getItem('emailStatus')==="true"){ return true; }
    else{ return false; }
  }

  setSession(authResult) {
    // Set isLoggedIn flag in localStorage
    localStorage.setItem('isLoggedIn', 'true');
    sessionStorage.setItem('isLoggedIn', 'true');
    
    // Set the time that the access token will expire at
    let expiresAt = (authResult.expiresIn * 1000) + new Date().getTime();
    console.log(expiresAt)

    //set everything in local storage, because this seems janky.
    this.expiresAt = expiresAt;
    localStorage.setItem('expiresAt', expiresAt);
    sessionStorage.setItem('expiresAt', expiresAt);

    this.accessToken = authResult.accessToken;
    localStorage.setItem('accessToken', authResult.accessToken);
    sessionStorage.setItem('accessToken', authResult.accessToken);

    this.idToken = authResult.idToken;
    localStorage.setItem('idToken', authResult.idToken);
    sessionStorage.setItem('idToken', authResult.idToken);

    //set email status in local storage
    var userInfo = this.getUserInfo();
    if (typeof(userInfo) !== 'undefined'){
      if ("email_verified" in userInfo){
        var status = userInfo.email_verified;
        localStorage.setItem('emailStatus', status);
        sessionStorage.setItem('emailStatus', status);
      }
      else{
        localStorage.setItem('emailStatus', false);
        sessionStorage.setItem('emailStatus', false);
      }
    }
    else{
      localStorage.setItem('emailStatus', false);
      sessionStorage.setItem('emailStatus', false);
    }

    // navigate to the home route
    history.replace('/home');
  }

  loadSession(){
    this.accessToken = sessionStorage.getItem('accessToken');
    this.idToken = sessionStorage.getItem('idToken');
    this.expiresAt = sessionStorage.getItem('expiresAt');
    this.email_verified = sessionStorage.getItem('emailStatus');
  }

  renewSession() {
    this.auth0.checkSession({}, (err, authResult) => {
       if (authResult && authResult.accessToken && authResult.idToken) {
         this.setSession(authResult);
       } else if (err) {
         this.logout();
         console.log(err);
         alert(`Could not get a new token (${err.error}: ${err.error_description}).`);
       }
    });
  }


  logout() {
    // Remove tokens and expiry time
    this.accessToken = null;
    this.idToken = null;
    this.expiresAt = 0;

    //clear localstorage
    localStorage.removeItem('expiresAt');
    localStorage.removeItem('accessToken');
    localStorage.removeItem('idToken');
    localStorage.removeItem('emailStatus');

    // Remove isLoggedIn flag from localStorage
    localStorage.removeItem('isLoggedIn');

    // navigate to the home route
    history.replace('/home');
  }

  isAuthenticated() {
    // Check whether the current time is past the
    // access token's expiry time
    //let expiresAt = this.expiresAt;
    //console.log(expiresAt);
    var stored_expiry = localStorage.getItem('expiresAt');
    console.log(stored_expiry);
    var login_status = localStorage.getItem('isLoggedIn');
    if ( login_status == "true" && (new Date().getTime() < stored_expiry)){
      return true;
    }
    else{
    console.log(login_status);
    console.log(new Date().getTime());
    console.log(Number(stored_expiry));
    return false;
    }
  }
}
