import jwt
import requests
import json
import os

validation_url = os.environ["validation_endpoint"]

def validate_jwt(jwt_string):
    try:
        keys = requests.get(validation_url).json()
    except Exception as e:
        print("Error getting validation url: "+ str(e))
    rsa_key = ''
    for key in keys['keys']:
        if key['alg'] == 'RSA256':
            rsa_key = key['x5c']
    try:
        decoded_jwt = jwt.decode(jwt_string, rsa_key, algorithm=['RS256'])
    except Exception as e:
        print("Error decoding key: "+ str(e))
        return False

def lambda_handler(event, context):
    print(event)
    validation_result = validate_jwt(event['headers']['authentication'])
    if validation_result is False:
        return {"statusCode":403, "body": json.dumps("JWT validation failed. Unauthorized.")}
    else:    
        json_data = {"menu":[{"item":{"name":"Pepperoni","description":"Pepperoni and Cheese on Red Sauce","price":2}},{"item":{"name":"Margherita","description":"Mozzarella and Basil on Red Sauce","price":2}},{"item":{"name":"Cheese","description":"Cheese on Red Sauce","price":2}},{"item":{"name":"Hawaiian","description":"Ham and Pineapple on Red Sauce","price":2}},{"item":{"name":"Potato","description":"Potato, Sage, and Cheese on Alfredo Base","price":2}},{"item":{"name":"Sausage","description":"Sausage and Cheese on Red Sauce","price":2}}]}
            
        return {"statusCode":200, "body": json_data}
