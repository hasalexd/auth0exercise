import jwt
import boto3
import requests
import json
import os

validation_url = os.environ("validation_endpoint")
bucket = os.environ("bucket")
key = os.environ("key")

client = boto3.client('s3')

def validate_jwt(jwt_string):
    keys = requests.get(validation_url).json()
    rsa_key = ''
    for key in keys['keys']:
        if key['alg'] == 'RSA256':
            rsa_key = key['x5c']
    try:
        decoded_jwt = jwt.decode(jwt_string, rsa_key, algorithm=['RS256'])
    except Exception as e:
        return False

def lambda_handler(event, context):
    validation_result = validate_jwt(event['authentication'])
    if validation_result is False:
        return {"statusCode":403, "body": json.dumps("JWT validation failed. Unauthorized.")}
    else:
        if event['input']['HTTPmethod'] == 'POST':
            try:
                print(event['input']['body'])
                
                return {"statusCode":200, "body": json.dumps("Cool, man. We got you.")
            except Exception as e:
                return {"statusCode": 500, "body": json.dumps("S3 access failed.")}
        else:
            print("wrong request type")
    
